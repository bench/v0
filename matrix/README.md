# Origine des matrices : Matrix Market

A voir les pointeurs pour avoir une idée de la structure des matrices.

Le code n'accepte actuellement que les matrices de format "general", i.e. le non non-symétrique.

Les matrices ci-dessous sont listées par leur complexité de la plus grande à la plus petite (+/-) :-)

* **e40r5000.mtx**
    * https://math.nist.gov/MatrixMarket/data/SPARSKIT/drivcav/e40r5000.html
* **af23560.mtx**
    * https://math.nist.gov/MatrixMarket/data/NEP/airfoil/af23560.html
* **psmigr_3.mtx**
    * https://math.nist.gov/MatrixMarket/data/Harwell-Boeing/psmigr/psmigr_3.html
* **memplus.mtx**
    * https://math.nist.gov/MatrixMarket/data/misc/hamm/memplus.html
* **cavity26.mtx**
    * https://math.nist.gov/MatrixMarket/data/SPARSKIT/drivcav_old/cavity26.html
* **mhd4800a.mtx**
    * https://math.nist.gov/MatrixMarket/data/NEP/mhd/mhd4800a.html
* **dw8192.mtx**
    * https://math.nist.gov/MatrixMarket/data/NEP/dwave/dw8192.html
* **mcfe.mtx**
    * https://math.nist.gov/MatrixMarket/data/Harwell-Boeing/astroph/mcfe.html
* **jpwh_991.mtx**
    * https://math.nist.gov/MatrixMarket/data/Harwell-Boeing/cirphys/jpwh_991.html
* **gre_1107.mtx**
    * https://math.nist.gov/MatrixMarket/data/Harwell-Boeing/grenoble/gre_1107.html
* **fs_541_4.mtx**
    * https://math.nist.gov/MatrixMarket/data/Harwell-Boeing/smtape/fs_541_4.html
