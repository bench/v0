# Hello
Il s'agit d'une compilation de codes académiques pouvant servir de benchmarks.
Ces codes sont articulés autour du produit matrice-vecteur creux en utilisant une
représentation [CSR](https://en.wikipedia.org/wiki/Sparse_matrix).

Contenu des petits répertoires :
* **utils**: lecture des matrices au format matrix-market.
* **matrix** : quelques matrices; sinon aller voir [MatrixMarket](https://math.nist.gov/MatrixMarket/applications.html) pour choisir
des domaines d'applications afin de récupérer quelques matrices
* **spmv** : la brique fondamentale, i.e. le Sparse Product Matrix-Vector avec des matrices au format dense (rappel) et au format CSR
* **euler** : une bonne vielle méthode Euler Explicite pour résoudre un système d'EDO Y'=Ay + les conditions de Cauchy y(0)=Y0
* **bicgstab** :  en gros le code direct du [BicGStab](https://en.wikipedia.org/wiki/Biconjugate_gradient_stabilized_method) mais sans les préconditionneurs.
Il s'agit essentiellement d'une séquence d'appels à SpMV
