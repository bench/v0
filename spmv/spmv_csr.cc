//
//  Created by Thierry on 05/12/2017.
//  Copyright © 2017 Gautier Thierry
//  Copyright © 2017 Roussel Adrien
//
//
#include <iostream>
#include "mmio.h"
#include "spmv_csr.h"


/* */
CSRSparseMatrix::~CSRSparseMatrix()
{
  if (NNZ==0) return;
  delete []data;
  delete []row_start;
  delete []col_idx;
}


/*
*/
CSRSparseMatrix& CSRSparseMatrix::operator=(const CSRSparseMatrix& v)
{
  double* dest = data;
  if (NNZ != v.NNZ)
  {
    delete [] data;
    data = new double[v.NNZ];
    delete [] col_idx;
    col_idx = new int[v.NNZ];
    delete [] row_start;
    row_start = new int[v.rows()];
    NNZ = v.NNZ;
  }
  for (int i=0; i<NNZ; ++i)
  {
    data[i] = v.data[i];
    col_idx[i] = v.col_idx[i];
  }
  for (int i=0; i<v.rows(); ++i)
  {
    row_start[i] = v.row_start[i];
  }
  dim[0] = v.dim[0];
  dim[1] = v.dim[1];
  return *this;
}


/*
*/
int CSRSparseMatrix::read( const std::string& filename)
{ 
  return mmoi_read_matrix_csr( filename.c_str(), dim, &NNZ, &data, &row_start, &col_idx ); 
}


/*
*/
int CSRSparseMatrix::write( const std::string& filename) const
{
  return mmoi_write_matrix_csr( filename.c_str(), dim, NNZ, data, row_start, col_idx ); 
  return 0;
}


/*
*/
int CSRSparseMatrix::pretty_print() const
{
  int N = rows();
  int M = cols();
  if ((N >16) || (M >16))
  {
    std::cout << "<Too large to print>\n";
    return 0;
  }
  /* assume col index sorted increasing order */
  for (int i = 0; i < rows(); i++)
  {
    int m = 0;
    if (row_start[i] == row_start[i+1]) /* empty row */
    {
      for (m = 0; m < M; ++m)
        std::cout << "0" << ' ';
      std::cout << std::endl;
    }
    for (int j = row_start[i];
         j < row_start[i+1]; j++)
    {
      for (; m < col_idx[j]; ++m)
        std::cout << "0" << ' ';
      std::cout << data[j] << ' ';
      ++m;
    }
    for (; m < M; ++m)
      std::cout << "0" << ' ';
    std::cout << std::endl;
  }
  return 0;
}



/*
*/
int CSRSparseMatrix::spmv( double* y, const double* x ) const
{
  int* rs = row_start;
  for (int i = 0; i < dim[0]; i++)
  {
    double c = 0;
    for (int j = rs[i]; j < rs[i+1]; j++)
      c += data[j] * x[col_idx[j]];
    y[i] = c;
  }
  return 0;
}



/*
*/
int CSRSparseMatrix::make_laplacian1D( int N )
{
  if (data) delete []data;
  if (row_start) delete []row_start;
  if (col_idx) delete []col_idx;
  NNZ = 3*N-2;
  data = new double[NNZ];
  row_start = new int[N+1];
  col_idx = new int[NNZ];
  row_start[0] = 0;
  int idx      = 0;
  for (int i=0; i<N; ++i)
  {
    if (i>0) { data[idx] = 1; col_idx[idx] = i-1; ++idx; }
    data[idx] = -2; col_idx[idx] = i; ++idx;
    if (i<N-1) { data[idx] = 1; col_idx[idx] = i+1; ++idx; }
    row_start[i+1] = idx;
  }
  dim[0] = dim[1] = N;
  return 0;
}


/*
*/
int CSRSparseMatrix::make_laplacian2D( int N )
{
  if (data) delete []data;
  if (row_start) delete []row_start;
  if (col_idx) delete []col_idx;
  NNZ = N*(5*N-4);
  data = new double[NNZ];
  row_start = new int[N*N+1];
  col_idx = new int[NNZ];
  row_start[0] = 0;
  int idx      = 0;
  for (int I=0; I<N; ++I)
  {
    int colD = I*N;
    if (I==0) { /* A I */
      for (int i=0; i<N; ++i)
      {
        /* A */
        if (i>0) { data[idx] = 1; col_idx[idx] = colD+i-1; ++idx; }
        data[idx] = -4; col_idx[idx] = colD+i; ++idx;
        if (i<N-1) { data[idx] = 1; col_idx[idx] = colD+i+1; ++idx; }
        /* I */
        data[idx] = 1; col_idx[idx] = colD+N+i ; ++idx;

        row_start[colD+i+1] = idx;
      }
    }
    else if (I==N-1) {  /* I A */
      for (int i=0; i<N; ++i)
      {
        /* I */
        data[idx] = 1; col_idx[idx] = colD-N+i ; ++idx;
        /* A */
        if (i>0) { data[idx] = 1; col_idx[idx] = colD+i-1; ++idx; }
        data[idx] = -4; col_idx[idx] = colD+i; ++idx;
        if (i<N-1) { data[idx] = 1; col_idx[idx] = colD+i+1; ++idx; }

        row_start[colD+i+1] = idx;
      }
    }
    else { /* I A I */
      for (int i=0; i<N; ++i)
      {
        /* I */
        data[idx] = 1; col_idx[idx] = colD-N+i ; ++idx;
        /* A */
        if (i>0) { data[idx] = 1; col_idx[idx] = colD+i-1; ++idx; }
        data[idx] = -4; col_idx[idx] = colD+i; ++idx;
        if (i<N-1) { data[idx] = 1; col_idx[idx] = colD+i+1; ++idx; }
        /* I */
        data[idx] = 1; col_idx[idx] = colD+N+i ; ++idx;

        row_start[colD+i+1] = idx;
      }
    }
  }
  dim[0] = dim[1] = N*N;
  return 0;
}

