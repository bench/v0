//
//  Created by Thierry on 05/12/2017.
//  Copyright © 2017 Gautier Thierry
//  Copyright © 2017 Roussel Adrien
//
//
#include <iostream>
#include <chrono>
#include <algorithm>
#include "spmv_csr.h"


/*
*/
int main(int argc, char** argv)
{
  if (argc <2) 
  {
    std::cerr << "Usage: " << argv[0] << " <input matrix>" << std::endl;
    return 0;
  } 

  CSRSparseMatrix mymat;
  if (mymat.read(argv[1]) !=0)
  {
    std::cerr << "*** error: cannot read '" << argv[1] << "'" << std::endl;
  }
  // debug here: to verify is input == output modulo re-ordering by sorting input with lexicographic (I,J) order
  mymat.write("whatread.mtx");

  double* x = new double[mymat.cols()];
  double* y = new double[mymat.cols()];
  for (int i=0; i<mymat.cols(); ++i)
  {
    x[i] = 0;
    y[i] = 0;
  }

  // methodo pourrie ici !
  auto start = std::chrono::steady_clock::now(); 
  mymat.spmv( y, x);
  auto end = std::chrono::steady_clock::now(); 

  std::cout << "done" << std::endl;
  std::cout << "Duration:" << std::chrono::duration <double, std::milli> (end-start).count() << " ms" << std::endl;

  delete []x;
  delete []y;
  return 0;
}
