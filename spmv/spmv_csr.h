//
//  Created by Thierry on 05/12/2017.
//  Copyright © 2017 Gautier Thierry
//  Copyright © 2017 Roussel Adrien
//
//
#ifndef __CSR_SPARSE_MAT_H_
#define __CSR_SPARSE_MAT_H_ 1
#include <iostream>
#include <chrono>

class CSRSparseMatrix {
public:
  /*
  */
  CSRSparseMatrix() : NNZ(0), data(0), row_start(0), col_idx(0)
  { dim[0] = dim[1] = 0; }

  /*
  */
  ~CSRSparseMatrix();

  /* Cstor + Copy 
  */
  CSRSparseMatrix(const CSRSparseMatrix& v);

  /* Assignement
  */
  CSRSparseMatrix& operator=(const CSRSparseMatrix& v);

  /* Read from file in matrix Market coordinate format
     Return 0 if no error
  */
  int read( const std::string& filename);

  /* Write file in matrix Market coordinate format
     Return 0 if no error
  */
  int write( const std::string& filename) const;

  /* Replot it using coordinate...
  */
  int pretty_print() const;

  /* */
  int rows() const { return dim[0]; }

  /* */
  int cols() const { return dim[1]; }

  /* From SparseMatrix 
  */
  int spmv( double* y, const double* x ) const;


  /* Utility: makes 3 points stencil sparse matrix over 1D grid of size N
  */
  int make_laplacian1D( int N );

  /* Utility: makes 5 points stencil sparse matrix over 2D grid of size NxN
  */
  int make_laplacian2D( int N );

protected:
  int     dim[2]; // dim[0] = M = #rows; dim[1] = N = #cols
  int     NNZ;    // number of non zero elements. 
  double* data;   // array of values of size NNZ
  int* row_start; // size == M+1. Values of row j are data[j] for j in [row_start[k]:row_start[k+1]) 
  int* col_idx;   // size NNZ. Value data[j] are at col position col_idx[j].
};

#endif

