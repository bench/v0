//
//  bicgstab.h
//  libomp
//
//  Created by Thierry on 05/12/2017.
//  Copyright © 2017 Gautier Thierry
//  Copyright © 2017 Roussel Adrien
//
// Idea: memory allocation by blocks. Task grain is fixed by block size.
// The goal is to test different way to reduce the work inflation of this bicgstab version
//

#include <iostream>
#include <random>
#include "linalg.h"

/*
*/
Vector& Vector::operator=(const Vector& v)
{
  if (data) delete []data;
  size = v.size;
  data = new double[size];
  assign(v);
  return *this;
}

/*
*/
double Vector::squared_norm() const
{
  if (size ==0) return 0;
  double d = 0;
  for (int i=0; i<size; ++i)
  {
    double tmp = data[i];
    d += tmp*tmp;
  }
  return d;
}

/*
*/
double Vector::dot(const Vector& v) const
{
  if ((size ==0) || (v.size ==0)) return 0;
  if (size != v.size) return 0; /* should abort */
  double d = 0;
  for (int i=0; i<size; ++i)
    d += data[i]*v.data[i];
  return d;
}

/*
*/
void Vector::set_zero() const
{
  if (size ==0) return;
  for (int i=0; i<size; ++i)
    data[i] = 0.0;
}

/*
*/
void Vector::set_one() const
{
  if (size ==0) return;
  for (int i=0; i<size; ++i)
    data[i] = 1.0;
}

/*
*/
void Vector::set_rand() const
{
  if (size ==0) return;

  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_real_distribution<> dist(-1.0, 1.0);

  for (int i=0; i<size; ++i)
    data[i] = dist(gen);
}

int Vector::pretty_print() const
{
  int end_seg1, beg_seg2;
  end_seg1 = beg_seg2 = size;
  if (size >16)
  {
    end_seg1 = 6;
    beg_seg2 = size-6;
  }
  for (int i=0; i<end_seg1; ++i)
    std::cout << data[i] << std::endl;
  if (end_seg1 != size)
    for (int i=0; i<3; ++i)
      std::cout << '.' << std::endl;
  for (int i=beg_seg2; i<size; ++i)
    std::cout << data[i] << std::endl;
  return 0;
}

/* assign: this <- x
*/
void Vector::assign( const Vector& x )
{
  for (int i=0; i<size; ++i)
    data[i] = x[i];
}

/*
*/
void Vector::swap( Vector& x )
{
  std::swap(data,x.data);
  std::swap(size,x.size);
}

/* sub: this <- x - y
*/
void Vector::sub( const Vector& x, const Vector& y )
{
  for (int i=0; i<size; ++i)
    data[i] = x[i]-y[i];
}

/* axpy: this <- a*x+y
*/
void Vector::axpy( double a, const Vector& x, const Vector& y )
{
  if (a== 0.0) assign(y);
  if (a== 1.0)
  {
    for (int i=0; i<size; ++i)
      data[i] = x[i]+y[i];
  }
  else if (a==-1.0)
  {
    for (int i=0; i<size; ++i)
      data[i] = -x[i]+y[i];
  }
  else {
    for (int i=0; i<size; ++i)
      data[i] = a*x[i]+y[i];
  }
}

/* y2+b*(y+a*x) */
void Vector::axpy2( double a, const Vector& x, const Vector& y, double b, const Vector& y2 )
{
  for (int i=0; i<size; ++i)
  {
    data[i] = y2.data[i] + b * (y.data[i] + a * x.data[i]);
  }
}

/* y+a1*x1 + a2*x2 */
void Vector::ax2py( double a1, const Vector& x1, double a2, const Vector& x2, const Vector& y )
{
  for (int i=0; i<size; ++i)
    data[i] = y.data[i] + a1 * x1.data[i] + a2 * x2.data[i];
}



void Vector::spmv( const CSRSparseMatrix& A, const Vector& v)
{
  if (A.cols() != dim()) 
  {
    std::cerr << __func__ << ":: Bad dimensions" << std::endl;
    abort();
  } 
  A.spmv( data, v.data );
}
