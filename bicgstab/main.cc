//
//  Created by Thierry on 05/12/2017.
//  Copyright © 2017 Gautier Thierry
//  Copyright © 2017 Roussel Adrien
//
//
#include <iostream>
#include <chrono>
#include <algorithm>
#include <random>
#include "spmv_csr.h"
#include "linalg.h"


/*
*/
int main(int argc, char** argv)
{
  if (argc <3) 
  {
    std::cerr << "Usage: " << argv[0] << " <input matrix> <tol>" << std::endl;
    return 0;
  } 

  CSRSparseMatrix A;
  if (A.read(argv[1]) !=0)
  {
    std::cerr << "*** error: cannot read '" << argv[1] << "'" << std::endl;
  }

  double tol_error = atof( argv[1] );
  int iters = 1000;

  Vector b(A.cols());
  Vector x(A.cols());

  x.set_zero();
  b.set_rand();
  
  // methodo pourrie ici !
  auto start = std::chrono::steady_clock::now(); 

  /* Bicgstab - more or less wikipedia vesion without preconditioning
     - cut and paste of older code -> should be in function
  */
  double tol = tol_error;
  int maxIters = iters;
  int n = A.cols();
  int retval = 0;

  double beta;
  double tmpr;
  double tmp;

  double rho    = 1;
  double alpha  = 1;
  double w      = 1;

  Vector r(x.dim());
  Vector r0(x.dim());

  Vector v (n); v.set_zero();
  Vector p (n); p.set_zero();
  Vector s(n), t(n);

  double tol2 = tol * tol;
  double eps2 = std::numeric_limits<double>::epsilon()*std::numeric_limits<double>::epsilon();
  int i = 0;
  int restarts = 0;

  double rhs_sqnorm;
  double r0_sqnorm;
  double r_sqnorm;

  r.spmv(A, x);
  r0.sub(b, r); /* r  = b - A * x; */
  r.assign(r0);

  r0_sqnorm  = r0.squared_norm( );
  rhs_sqnorm = b.squared_norm( );
  if (rhs_sqnorm == 0)
  {
    x.set_zero();
    retval = 1;
    goto return_label;
  }
  r_sqnorm = r0_sqnorm;

  while ( r_sqnorm/rhs_sqnorm > tol2 && i<maxIters )
  {
    double rho_old = rho;
    rho = r0.dot(r);

    beta = (rho/rho_old) * (alpha/w);
    p.axpy2( -w, v, p, beta, r); /* r+beta(p-w*v) */

    /*y = precond.solve(p); */
    v.spmv( A, p ); 
    tmpr = r0.dot(v);

    alpha = rho / tmpr ;
    s.axpy( -alpha, v, r); /* r - alpha * v */

    /*z = precond.solve(s);*/
    t.spmv( A, s); /* t = mat * z;*/
    tmp = t.squared_norm();
    if (tmp > double(0))
      w = t.dot(s) / tmp;
    else
      w = double(0);
 
    /* if precond is implemented => p should be y and s should be z */
    x.ax2py( alpha, p, w, s, x ); /* x<-alpha*y + w * s +y*/
    r.axpy( -w, t, s ); /* s - w * t */
    r_sqnorm = r.squared_norm( );

    ++i;
  }

return_label:
  tol_error = sqrt(r_sqnorm/rhs_sqnorm);
  iters = i;

  auto end = std::chrono::steady_clock::now(); 

  std::cout << "done" << std::endl;
  std::cout << "Tolerance : " << tol_error << std::endl;
  std::cout << "#iteration: " << iters << std::endl;
  std::cout << "Duration  : " << std::chrono::duration <double, std::milli> (end-start).count() << " ms" << std::endl;

  return 0;
}
