//
//  bicgstab.h
//  libomp
//
//  Created by Thierry on 05/12/2017.
//  Copyright © 2017 Gautier Thierry
//  Copyright © 2017 Roussel Adrien
//
// Idea: memory allocation by blocks. Task grain is fixed by block size.
// The goal is to test different way to reduce the work inflation of this bicgstab version
//
#ifndef __LINALG__H__
#define __LINALG__H__ 1

#include "spmv_csr.h"

class Vector{
protected:
  int size;
  double* data;

  /*
  */
  Vector( double* d, int n ) : size(n), data(d)
  {}

public:
  /*
  */
  Vector() : size(0), data(0)
  { }

  /*
  */
  Vector(int size) : size(size), data(0)
  { 
    data = new double[size];
  }

  int dim() const { return size; }

  /*
  */
  double operator[](int i) const
  { return data[i]; }

  /*
  */
  double& operator[](int i)
  { return data[i]; }


  /*
  */
  Vector& operator=(const Vector& v);
  
  /*
  */
  double squared_norm() const;
  
  /*
  */
  double norm() const
  { return sqrt( squared_norm() ); }

  /*
  */
  double dot(const Vector& v) const;
  
  /*
  */
  void set_zero() const;
  
  /*
  */
  void set_one() const;
  
  /*
  */
  void set_rand() const;
  
  /*
  */
  int pretty_print() const;
  
  /* assign: this <- x
  */
  void assign( const Vector& x );
  
  /*
  */
  void swap( Vector& x );
  
  /* sub: this <- x - y
  */
  void sub( const Vector& x, const Vector& y );
  
  /* axpy: this <- a*x+y
  */
  void axpy( double a, const Vector& x, const Vector& y );
  
  /* y2+b*(y+a*x) */
  void axpy2( double a, const Vector& x, const Vector& y, double b, const Vector& y2 );
  
  /* y+a1*x1 + a2*x2 */
  void ax2py( double a1, const Vector& x1, double a2, const Vector& x2, const Vector& y );

  /* A*v */
  void spmv( const CSRSparseMatrix& A, const Vector& v);
};

#endif

