//
//  Created by Thierry on 05/30/2020.
//
//
#include <iostream>
#include <chrono>
#include <random>
#include "spmv_csr.h"


/*
*/
int main(int argc, char** argv)
{
  if (argc <3) 
  {
    std::cerr << "Usage: " << argv[0] << " <input matrix> <step>" << std::endl;
    return 0;
  } 

  CSRSparseMatrix mymat;
  if (mymat.read(argv[1]) !=0)
  {
    std::cerr << "*** error: cannot read '" << argv[1] << "'" << std::endl;
  }
  int N = atoi(argv[2]);
  if (N<1) N = 1;

  double h = 1e-3;

  double* x0 = new double[mymat.cols()];
  double* x1 = new double[mymat.cols()];
  double* y = new double[mymat.cols()];

  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_real_distribution<> dist(-1.0, 1.0);

  for (int i=0; i<mymat.cols(); ++i)
  {
    x0[i] = dist(gen);
    x1[i] = y[i] = 0;
  }

  // methodo pourrie ici !
  auto start = std::chrono::steady_clock::now(); 

  // time loop
  for (int i=0; i<N; ++i)
  {
    // y = A*x0
    mymat.spmv( y, x0);

    // here composition of axpy(h,y,x0) -> x1 ; x1.swap(x0);
    for (int k=0; k< mymat.cols(); ++k)
    {
      x1[k] = x0[k] + h*y[k];
      x0[k] = x1[k];
    }
  }
  auto end = std::chrono::steady_clock::now(); 

  std::cout << "done" << std::endl;
  std::cout << "Duration:" << std::chrono::duration <double, std::milli> (end-start).count() << " ms" << std::endl;

  delete []x0;
  delete []x1;
  delete []y;
  return 0;
}
