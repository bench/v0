//
//  mmio_read_sparse
//
//
#ifndef __MMIO__READ__SPARSE__H__
#define __MMIO__READ__SPARSE__H__ 1

#include "mmio.h"
#include <algorithm>
#include <cstring>


/* Read Matrix Market sparse format and return CSR information
*/
int mmoi_read_matrix_csr( const char* filename, int* dim, int* nzero, double** val, int** row_start, int** col_idx )
{
  /* read in coordinate representation then convert to csr */
  double* data =0;
  int* I =0; int* J =0;
  int NNZ, M, N;
  int err = mm_read_unsymmetric_sparse( filename, &M, &N, &NNZ, &data, &I, &J );
  if (err !=0) return err;

  /* Memory space complexity in 3*NNZ... */
  int* perm = (int*)malloc( sizeof(int)*NNZ );
  for (int i=0; i<NNZ; ++i)
    perm[i] = i;

  /* sort (data,I,J) according to increasing lexicographic I,J */
  std::sort(perm, perm+NNZ, [&](const int& a, const int& b) {
        if (I[a] < I[b]) return true;
        if (I[a] > I[b]) return false;
        return J[a] < J[b];
  });

  int* nJ = (int*)malloc( sizeof(int)*NNZ);
  double* ndata = (double*)malloc( sizeof(double)*NNZ);
  for (int i=0; i<NNZ; ++i)
  {
    ndata[i] = data[perm[i]];
    nJ[i] = J[perm[i]];
  }

  /* create  row_start */
  int* row_s = (int*)malloc(sizeof(int)* (M+1) );
  int nr = 0;
  row_s[0] = 0;
  for (int i=1; i<NNZ; ++i)
  {
    if (I[perm[i]] != row_s[nr])
    {
      for (int k=nr+1; k<I[perm[i]]; ++k)
        row_s[k] = row_s[nr];
      nr = I[perm[i]];
      row_s[nr] = i;  
    }
  }
  row_s[N] = NNZ;
  *nzero = NNZ;
  dim[0] = M;
  dim[1] = N;
  *val = ndata;
  *row_start = row_s;
  *col_idx = nJ;

  free(I);
  free(J);
  free(data);
  free(perm);

  return 0;
}


/* */
int mmoi_write_matrix_csr( const char* filename, const int* dim, const int NNZ, const double* data, const int* row_start, const int* col_idx )
{
  int err;
  FILE* f;
  MM_typecode matcode;

  if (strcmp(filename, "stdout") == 0)
    f = stdout;
  else
  if ((f = fopen(filename, "w")) == NULL)
    return MM_COULD_NOT_WRITE_FILE;

  mm_clear_typecode(&matcode);
  mm_set_matrix(&matcode);
  mm_set_sparse(&matcode);
  mm_set_real(&matcode);
  mm_set_general(&matcode);

  err = mm_write_banner(f, matcode );
  if (err !=0) return err;


  /* print matrix sizes and nonzeros */
  fprintf(f, "%d %d %d\n", dim[0], dim[1], NNZ);

  int nnz = 0;
  for (int i=0; i<dim[0]; ++i)
  {
    for (int k=row_start[i]; k<row_start[i+1]; ++k, ++nnz)
      fprintf(f, "%d %d %20.16g\n", i+1, 1+col_idx[nnz], data[k]);
  }
  if (nnz != NNZ) return MM_UNSUPPORTED_TYPE;

  if (f !=stdout) fclose(f);
  return 0;
}
#endif
